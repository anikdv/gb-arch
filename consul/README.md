## Создание сайта

Создан сайт в соответствии с методическими указаниями

Сайт собран командой docker-compose build:

```
Successfully built 688e78318238
Successfully tagged web_consul:latest
```

## Запуск сервисов

Сервисы были успешно запущены командой `docker-compose up` (см [consul.png](consul.png) и [fabio.png](fabio.png))


## Расширение сети

Был получен список сетей докера (команда ихз методических указаний - не подошла):

``` > docker network ls```

```
NETWORK ID     NAME                    DRIVER    SCOPE
aa53ead4d5fa   bridge                  bridge    local
9cb747a43cd2   consul_consul_network   bridge    local
87a5a812eb38   host                    host      local
3fac83d12bc7   none                    null      local
```

Были добавлены дополнительные контейнеры  для сети consul_consul_network (см. скрипт [add-applications.sh](add-applications.sh))


Дополнительные контейнеры видны на рисунках [consul-2.png](consul-2.png) и [fabio-2.png](fabio-2.png)
