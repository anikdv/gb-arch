docker run -d --network=consul_consul_network --ip="33.10.0.13" -p 8083:80 --env NODE=web_3 --env PRIVATE_IP_ADDRESS=33.10.0.13 --env LOAD_BALANCER=33.10.0.100 web_consul:latest

docker run -d --network=consul_consul_network --ip="33.10.0.14" -p 8084:80 --env NODE=web_4 --env PRIVATE_IP_ADDRESS=33.10.0.14 --env LOAD_BALANCER=33.10.0.100 web_consul:latest
