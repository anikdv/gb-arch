# Был создан докер контейнер для доменной службы

Файл располагается здесь: [domain-docker-file](server/domain/dockerfile)

Лог выполнения приведен ниже:

```
master !2 ?1>  docker build -t gb-arch-domain .
Sending build context to Docker daemon  13.32MB
Step 1/14 : FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
 ---> 09231bfea5df
Step 2/14 : WORKDIR /app
 ---> Running in fdd22f4b96ef
Removing intermediate container fdd22f4b96ef
 ---> c98cc208b320
Step 3/14 : EXPOSE 80
 ---> Running in 9bf9ef466a7d
Removing intermediate container 9bf9ef466a7d
 ---> 4eb2c72ed916
Step 4/14 : EXPOSE 443
 ---> Running in 726786348e7d
Removing intermediate container 726786348e7d
 ---> 0940b14437d7
Step 5/14 : FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
 ---> 05ac491d1df4
Step 6/14 : WORKDIR /src
 ---> Running in 17520d9a6997
Removing intermediate container 17520d9a6997
 ---> 284139f3203d
Step 7/14 : COPY . /src
 ---> c2cfaa082c4e
Step 8/14 : RUN dotnet build "domain.csproj" -c Release -o /app/build
 ---> Running in b8542871b947
Microsoft (R) Build Engine version 17.0.0+c9eb9dd64 for .NET
Copyright (C) Microsoft Corporation. All rights reserved.

  Determining projects to restore...
  Restored /src/domain.csproj (in 2.55 sec).
  domain -> /app/build/domain.dll

Build succeeded.
    0 Warning(s)
    0 Error(s)

Time Elapsed 00:00:04.63
Removing intermediate container b8542871b947
 ---> a4b525a3082c
Step 9/14 : FROM build AS publish
 ---> a4b525a3082c
Step 10/14 : RUN dotnet publish "domain.csproj" -c Release -o /app/publish
 ---> Running in 774afdf4ddda
Microsoft (R) Build Engine version 17.0.0+c9eb9dd64 for .NET
Copyright (C) Microsoft Corporation. All rights reserved.

  Determining projects to restore...
  All projects are up-to-date for restore.
  domain -> /src/bin/Release/net6.0/domain.dll
  domain -> /app/publish/
Removing intermediate container 774afdf4ddda
 ---> a481204a1f91
Step 11/14 : FROM base AS final
 ---> 0940b14437d7
Step 12/14 : WORKDIR /app
 ---> Running in 2f22664b953c
Removing intermediate container 2f22664b953c
 ---> 54f2a4c1ffcd
Step 13/14 : COPY --from=publish /app/publish .
 ---> 43bf075eac7a
Step 14/14 : ENTRYPOINT ["dotnet", "domain.dll"]
 ---> Running in a3b9370e1f65
Removing intermediate container a3b9370e1f65
 ---> dd24d590d2e7
Successfully built dd24d590d2e7
Successfully tagged gb-arch-domain:latest
```
```
master !2 ?1>  docker images
REPOSITORY                        TAG       IMAGE ID       CREATED         SIZE
<none>                            <none>    a481204a1f91   6 seconds ago   773MB
gb-arch-domain                    latest    dd24d590d2e7   6 seconds ago   212MB
mcr.microsoft.com/dotnet/sdk      6.0       05ac491d1df4   3 days ago      715MB
mcr.microsoft.com/dotnet/aspnet   6.0       09231bfea5df   3 days ago      208MB
```
```
master !2 ?1>  docker run -d -p 8080:80 --name test gb-arch-domain
f30826a3816cf91c12f84ddeae0678ca32c447a6f45fc3ecc33c56bcb2c3a009
```
```
master !2 ?1>  docker stop test
test
```
```
master !2 ?1>  docker start test -i
{"EventId":14,"LogLevel":"Information","Category":"Microsoft.Hosting.Lifetime","Message":"Now listening on: http://[::]:80","State":{"Message":"Now listening on: http://[::]:80","address":"http://[::]:80","{OriginalFormat}":"Now listening on: {address}"}}
{"EventId":0,"LogLevel":"Information","Category":"Microsoft.Hosting.Lifetime","Message":"Application started. Press Ctrl\u002BC to shut down.","State":{"Message":"Application started. Press Ctrl\u002BC to shut down.","{OriginalFormat}":"Application started. Press Ctrl\u002BC to shut down."}}
{"EventId":0,"LogLevel":"Information","Category":"Microsoft.Hosting.Lifetime","Message":"Hosting environment: Production","State":{"Message":"Hosting environment: Production","envName":"Production","{OriginalFormat}":"Hosting environment: {envName}"}}
{"EventId":0,"LogLevel":"Information","Category":"Microsoft.Hosting.Lifetime","Message":"Content root path: /app/","State":{"Message":"Content root path: /app/","contentRoot":"/app/","{OriginalFormat}":"Content root path: {contentRoot}"}}
{"EventId":3,"LogLevel":"Warning","Category":"Microsoft.AspNetCore.HttpsPolicy.HttpsRedirectionMiddleware","Message":"Failed to determine the https port for redirect.","State":{"Message":"Failed to determine the https port for redirect.","{OriginalFormat}":"Failed to determine the https port for redirect."}}
^C{"EventId":0,"LogLevel":"Information","Category":"Microsoft.Hosting.Lifetime","Message":"Application is shutting down...","State":{"Message":"Application is shutting down...","{OriginalFormat}":"Application is shutting down..."}}
```

# Аналогично настроен проект для доступа к данным

Файл располагается здесь: [repo-docker-file](server/repository/dockerfile)

После настройки проектов контейнеры выглядят следующим образом:
```
master !2 ?5>  docker ps
CONTAINER ID   IMAGE            COMMAND                  CREATED          STATUS          PORTS                                            NAMES
09389fd11983   gb-arch-repo     "dotnet repository.d…"   29 seconds ago   Up 28 seconds   443/tcp, 0.0.0.0:9090->80/tcp, :::9090->80/tcp   gb-arch-repo
f30826a3816c   gb-arch-domain   "dotnet domain.dll"      10 minutes ago   Up 2 seconds    443/tcp, 0.0.0.0:8080->80/tcp, :::8080->80/tcp   test
```
В обоих контейнерах проект привязан к 80 порту, при этом при старте контейнера домену указывается 8080 порт, а репозиторию - 9090

# На последнем этапе собран контейнер с UI

Docker файл располагается здесь: [ui-docker-file](UI/web/gb-web/dockerfile)

После запуска всех трех проектов картина следующая:
```
master !2 ?5>  docker ps
CONTAINER ID   IMAGE            COMMAND                  CREATED          STATUS          PORTS                                            NAMES
436612975edb   gb-arch-ui       "docker-entrypoint.s…"   3 seconds ago    Up 2 seconds    0.0.0.0:10010->8080/tcp, :::10010->8080/tcp      gb-arch-ui
09389fd11983   gb-arch-repo     "dotnet repository.d…"   17 minutes ago   Up 17 minutes   443/tcp, 0.0.0.0:9090->80/tcp, :::9090->80/tcp   gb-arch-repo
f30826a3816c   gb-arch-domain   "dotnet domain.dll"      28 minutes ago   Up 17 minutes   443/tcp, 0.0.0.0:8080->80/tcp, :::8080->80/tcp   test
```
