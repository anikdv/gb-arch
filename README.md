# GeekBrains test project

The project consist of single ui and two server-side projects.

UI is vue3 application

server/domain contains .net6.0 web API project. It consumes repository business objects via gRPC, manipulates this objects internally and transfer manipulation result to web with RESTful API.

server/repository contains .net6.0 gRPC project. Repository is a CRUD-service, that stores data in several storages (like postgresql or mongodb). RDBMS access is perfomed via EntityFramework ORM. For RDB management are used EF built-in migrations
