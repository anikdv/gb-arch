### Создание кластера 

Был создан кластер в VK Cloud (см. скрин [cluster-init](deploy/k8s/cluster-init.png))

Полученный yaml-файл конфигурации был размещен в текущей директории и создан скрипт [set-config](deploy/k8s/set-config.sh) для создания переменой среды KUBECONFIG

После выполнения команды `kubectl proxy` по адресу 127.0.0.1:8001 получен интерфейс (см. скрин [cluster-api](deploy/k8s/cluster-api.png)) и доступна следующая конфигурация нодов:

`NAME                                      STATUS   ROLES    AGE     VERSION`

`kubernetes-cluster-2589-default-group-0   Ready    <none>   7h43m   v1.21.4`

`kubernetes-cluster-2589-master-0          Ready    master   7h48m   v1.21.4`

## Создание тестового pod-файла

Создан тестовый pod-файл ([test-pod](deploy/k8s/test-pod.yml)) и установлен командой `kubectl apply -f test-pod.yml`
После успешного деплоя картина следующая (команда kubectl get pod):

`NAME   READY   STATUS             RESTARTS   AGE`

`demo   0/1     CrashLoopBackOff   9          24m`

После тестирования pod был удален комндой `kubectl delete -f test.pod.yml`

## Создание релизного pod-файла

Создан релизный pod-файл ([pod](deploy/k8s/pod.yml)) и также установлен командой `apply`:

`NAME      READY   STATUS    RESTARTS   AGE`

`gb-arch   0/3     Pending   0          86s`

## Создание сервиса

Сервис сконфигурирован для web-приложения и располагается в файле [service](deploy/k8s/service.yml)

Статус службы следующий (получен командой `kubectl describe services/gb-arch-service`)

`Name:              gb-arch-service`

`Namespace:         default`

`Labels:            <none>`

`Annotations:       <none>`

`Selector:          app=gb-arch`

`Type:              ClusterIP`

`IP Family Policy:  SingleStack`

`IP Families:       IPv4`

`IP:                10.254.201.168`

`IPs:               10.254.201.168`

`Port:              http  5002/TCP`

`TargetPort:        5002/TCP`

`Endpoints:         <none>`

`Session Affinity:  None`

`Events:            <none>`

## Проблемы

К сожалению, как я не пытался достучаться к развернотому сервису из вне так и не получилось

Что я делаю не верно? :(

## UPD - работа над ошибками

Извне настроить доступ пока не получается, однако есть позитивные изменения.

Во-первых, была проблема с размещением конфигурационного файла для доступа к серверу. Поэтому теперь все команды выполняются алиасом:

`alias kubectl-vk="sudo kubectl --kubeconfig $HOME/dev/gb/gb-arch/deploy/k8s/kubernetes-cluster-2589_kubeconfig.yaml"`

По какой-то причине из $KUBECONFIG брать не хочет...

Во-вторых, была проблема с развертыванием подов: не скачивался образ. Образ добавлен через docker-hub и работает теперь корректно:

```
NAME          READY   STATUS    RESTARTS   AGE
gb-arch-pod   1/1     Running   0          5m36s
```

В-третьих, для тестирования откопана команда `kubectl-vk port-forward service/gb-arch-service 5002`, которая позволяет открыть развернутый сайт по адресу 127.0.0.1:5002

Вопрос с проброской через внешний IP остается открытым. В VK clouds мастер-серверу установлен ip 146.185.240.147, но сервис для него не работает (хотя внешний IP у службы стоит).
Адрс из конфига - 89.208.229.62 также завести не удается.
