## Создание yaml-файла

Создан файл [nginx.yml](nginx.yml) и установлен командон `apply`

После создания имеются следюущие поды в пространстве ingress-nginx (`kubectl-vk get pods --namespace=ingress-nginx`):

```
NAME                                             READY   STATUS    RESTARTS   AGE
ingress-nginx-controller-54684d99fd-8fqw4        1/1     Running   0          2m17s
ingress-nginx-default-backend-59fd879fd5-dzs2q   1/1     Running   0          2d12h
```

Далее осуществлена настройка ingress с помощью файла [ingress.yml](ingress.yml)


Созданный ingress (`kubectl-vk describe ingress/gb-arch-service-ingress`) представлен следующим образом:

```
Name:             gb-arch-service-ingress
Labels:           <none>
Namespace:        default
Address:          89.208.229.117
Default backend:  default-http-backend:80 (<error: endpoints "default-http-backend" not found>)
Rules:
  Host        Path  Backends
  ----        ----  --------
  *           
              /   gb-arch-service:5002 (10.100.4.7:8080)
Annotations:  <none>
Events:
  Type    Reason  Age                   From                      Message
  ----    ------  ----                  ----                      -------
  Normal  Sync    4m8s (x2 over 4m21s)  nginx-ingress-controller  Scheduled for sync
```

В результате настроек сайт открывается по внешнему IP (УРА!!!) (см. рисунок [vue-ui.png](vue-ui.png)).
